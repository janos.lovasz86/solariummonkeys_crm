trigger QuoteLineItemTrigger on QuoteLineItem (before insert, before update, after insert, after update, after delete) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            QuoteLineItemTriggerHandler.handleBeforeInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            QuoteLineItemTriggerHandler.handleBeforeUpdate(Trigger.oldMap, Trigger.New);
        }        
    }else if(Trigger.isAfter){
        if(Trigger.isInsert){
            QuoteLineItemTriggerHandler.handleAfterInsert(Trigger.New);
        }else if(Trigger.isUpdate){
            QuoteLineItemTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.New);
        }else if(Trigger.isDelete){
            QuoteLineItemTriggerHandler.handleAfterDelete(Trigger.old);
        }       
    }

}