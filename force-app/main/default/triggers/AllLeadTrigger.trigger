Trigger AllLeadTrigger on Lead (after insert){

    List<Lead> autoConvertLeadList = new List<Lead>();
    for (Lead l : Trigger.New){
            autoConvertLeadList.add(l);
    }
    if (!autoConvertLeadList.isEmpty()){
        AllLeadTriggerHandler.ConvertLeads(autoConvertLeadList);
    }
}