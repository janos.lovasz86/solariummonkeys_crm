/*
Task objektum Status mezője lehet Not Started, In Progress, Completed vagy Deferred. Aikor létrejön akkor Not Started.
Amikor létrejön egy Task objektum, akkor az tartalmaz egy Quote_For_Task__c custom lookup mezőt, ami az adott Quote Id-ja,
és egy Asset_For_Task__c custom lookup mezőt, ami az adott Asset Id-ja.
Fontos, hogy vagy a Quote-ra mutató Id-t, vagy az Asset-ra mutató Id-t tartalmazhatja, mindkettőt egyszerre nem.
Ebből különböztetjük meg, hogy Quote vagy Asset miatt jött létre a Task.

Használati esetek:
ApproveQuoteTask: (Ezt a Task-ot az OpportunityTriggerHandler hozza létre)
Ha az Opportunity Stage mezője WaitingForApprove értékű, a Task objektum Quote_For_Task__c custom lookup mezője nem üres, és a Task objektum Status mezője Completed, 
akkor a Quote objektum Status mezője Accepted lesz és az Opportunity Stage mezője Proposal/Price Quote értékbe vált.
Ha a Task objektum Status mezője Deferred, akkor a Quote Status objektum mezője Denied lesz.

CallClientForOrderTask: (Ezt a Task-ot egy időzitett osztály hozza létre)
Ha az Opportunity Stage mezője Approved értékű, és a Task objektum Status mezője Completed, akkor az Opportunity Stage mezője Order értékbe vált.
Ha az Opportunity Stage mezője Approved értékű, és a Task objektum Status mezője Deferred, akkor az Opportunity Stage mezője ClosedLost értékbe vált.
Task objektum Description mezője kitölthető, átmásoljuk az Opportunity Description mezőjébe.

CallClientForPaidTask: (Ezt a Task-ot egy időzitett osztály hozza létre)
Ha az Opportunity Stage mezője Order értékű, és a Task objektum Status mezője Completed, akkor az Opportunity Stage mezője Paid értékbe vált.
Ha az Opportunity Stage mezője Order értékű, és a Task objektum Status mezője Deferred, akkor az Opportunity Stage mezője ClosedLost értékbe vált.
Task objektum Description mezője kitölthető, átmásoljuk az Opportunity Description mezőjébe.

DeliverAssetTask: (Ezt a Task-ot az OpportunityTriggerHandler hozza létre)
Ha az Opportunity Stage mezője Paid értékű, a Task objektum Asset_For_Task__c custom lookup mezője nem üres, és a Task objektum Status mezője Completed, 
akkor az Opportunity Stage mezője Delivered értékbe vált.

GetBackAssetTask: (Ezt a Task-ot egy időzitett osztály hozza létre)
Ha az Opportunity Stage mezője Delivered vagy Closed Won értékű, a Task objektum Asset_For_Task__c custom lookup mezője nem üres, 
az aktuális dátum  egyezik a UsageEndDate-tel vagy nagyobb, és a Task objektum Status mezője Completed, 
akkor az Asset custom IsReturned__c mezője true értékbe vált.
*/
public class TasskTriggerHandler {


    public static void handleBeforeInsert(List<Task> triggerNew){
        //Task csak úgy jöhet létre, ha a Status mezöje Draft értékű.
	
    }
    
    
    public static void handleBeforeUpdate(Map<Id, Task> triggerOldMap, List<Task> triggerNew){
		
    }
    
    
    public static void handleAfterInsert(List<Task> triggerNew){        

    }
    
    
    public static void handleAfterUpdate(Map<Id, Task> triggerOldMap, List<Task> triggerNew){
        
	  	Date payDeadline = System.today();
        Set<Id> affectedQuoteIds = new Set<Id>();
        Set<Id> affectedQuoteRelatedAssetIds = new Set<Id>();
    	for(Task task : triggerNew) {
            if(task.Status != triggerOldMap.get(task.id).Status){//Ha változott a Task Status mezője
                if(task.Quote_For_Task__c != null){//Ha ApproveQuoteTask
                    affectedQuoteIds.add(task.Quote_For_Task__c);
                }else if(task.Asset_For_Task__c != null){//Ha DeliverAssetTask vagy GetBackAssetTask
                    affectedQuoteRelatedAssetIds.add(task.Quote_For_Task__c);
                }
                
            }
        }
        Map<Id,Quote> affectedQuotesByIds = new Map<Id,Quote>([SELECT Id, Status, Opportunity.StageName FROM Quote WHERE Id IN :affectedQuoteIds]);
        Map<Id,Quote> affectedQuotesRelatedAssetByIds = new Map<Id,Quote>([SELECT Id, Status, OpportunityId, Opportunity.StageName FROM Quote WHERE Id IN :affectedQuoteRelatedAssetIds]);
        
        Set<Id> oppIds = new Set<Id>();
        for(Id qId : affectedQuotesRelatedAssetByIds.keySet()){
            oppIds.add(affectedQuotesRelatedAssetByIds.get(qId).OpportunityId);
        }
        Map<Id,Asset> affectedAssetByIds = new Map<Id,Asset>([SELECT Id, OwnerId, UsageEndDate FROM Asset WHERE Id IN :oppIds]);
        
        for(Task task : triggerNew) {
            if(affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName == 'WaitingForApprove'){//ApproveQuoteTask
                if(task.Status == 'Completed'){
                    affectedQuotesByIds.get(task.Quote_For_Task__c).Status = 'Accepted';
                    affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName = 'Approved';
                }else if (task.Status == 'Deferred'){
                    affectedQuotesByIds.get(task.Quote_For_Task__c).Status = 'Denied';
                }                
            }else If(affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName == 'Approved'){//CallClientForOrderTask
                if(task.Status == 'Completed'){
                    affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName = 'Order';
                }else if (task.Status == 'Deferred'){
                    affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName = 'Closed Lost';
                } 
            }else If(affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName == 'Order'){//CallClientForPaidTask
                if(task.Status == 'Completed'){
                    affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName = 'Paid';
                }else if (task.Status == 'Deferred'){
                    affectedQuotesByIds.get(task.Quote_For_Task__c).Opportunity.StageName = 'Closed Lost';
                }
            }else If(affectedQuotesRelatedAssetByIds.get(task.Asset_For_Task__c).Opportunity.StageName == 'Paid'){//DeliverAssetTask
                if(task.Status == 'Completed'){
                    affectedQuotesRelatedAssetByIds.get(task.Asset_For_Task__c).Opportunity.StageName = 'Delivered';
                }
            }else If(affectedQuotesRelatedAssetByIds.get(task.Asset_For_Task__c).Opportunity.StageName == 'Delivered'){//GetBackAssetTask
                if(task.Status == 'Completed'){
                    for(Id assetId : affectedAssetByIds.keySet()){
                        if(affectedQuotesRelatedAssetByIds.get(task.Asset_For_Task__c).OpportunityId == affectedAssetByIds.get(assetId).Id){
                            affectedAssetByIds.get(assetId).IsReturned__c = true;
                        }
                    }
                }
            }            
        }
        update affectedQuotesByIds.values();
        update affectedQuotesRelatedAssetByIds.values();
        update affectedAssetByIds.values();
    }
    
    
    public static void handleAfterDelete(List<Task> triggerOld){

    }

 
    
}
