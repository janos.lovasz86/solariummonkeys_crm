public class AllLeadTriggerHandler{

    public static void ConvertLeads (List<Lead> leadList)    {
        if (leadList.isEmpty())        {
            return;
        }
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=True LIMIT 1];

        List<Database.LeadConvert> lcList = new List<Database.LeadConvert>();
        for (Lead l : leadList){
            if (l.IsConverted)            {
                continue;
            }
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.Id);

            String opName = l.Company==null?'':l.Company
                            + ' - '
                            + l.FirstName==null?'':l.FirstName
                            + ' '
                            + l.LastName==null?'':l.LastName;
            opName = opName.left(120).trim();
            lc.setOpportunityName(opName);
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lcList.add(lc);
        }


        List<Database.LeadConvertResult> lcrList;
        if ( ! lcList.isEmpty())        {
            lcrList = Database.convertLead(lcList, False);
        }
        for (Database.LeadConvertResult lcr : lcrList)        {
            System.Debug('@@@@@@@@'+lcr);
        }
    }
}
